
data "aws_ami" "amzn-linux-2023-ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-x86_64"]
  }
}
resource "aws_instance" "Frontend" {
  ami                         = data.aws_ami.amzn-linux-2023-ami.id
  instance_type               = "t2.micro"
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.ssh.id]
  key_name                    = "devops"
  user_data                   = <<-EOT
    #cloud-config
    package_update: true
    package_upgrade: true
    package_reboot_if_required: true
    swap:
      filename: /swapfile
      size: 8G
      maxsize: 8G
    mounts:
      - ["swap", "none", "swap", "sw", "0", "0"]
    packages:
      - docker
      - git
      - mc
      - zip
      - unzip
      - amazon-ecr-credential-helper
      - python3-pip
    
    groups:
      - docker  

    system_info:
      default_user:
        groups: [ docker ]
    
    runcmd:
     - systemctl enable docker
     - systemctl start docker
     - pip3 install --ignore-installed docker-compose 

    hostname:  frontend
    fqdn:  frontend
    prefer_fqdn_over_hostname: true

    final_message: "The system is finally up, after $UPTIME seconds"
  EOT
  tags = {
    Name = "Frontend"
  }
}


resource "aws_instance" "Backend" {
  ami                         = data.aws_ami.amzn-linux-2023-ami.id
  instance_type               = "t3.medium"
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.ssh.id]
  key_name                    = "devops"
  user_data                   = <<-EOT
    #cloud-config
    package_update: true
    
    package_upgrade: true
    package_reboot_if_required: true
    swap:
      filename: /swapfile
      size: 8G
      maxsize: 8G
    mounts:
      - ["swap", "none", "swap", "sw", "0", "0"]
    packages:
      - docker
      - git
      - mc
      - zip
      - unzip
      - amazon-ecr-credential-helper
      - python3-pip

    groups:
      - docker  

    system_info:
      default_user:
        groups: [ docker ]

    runcmd:
     - systemctl enable docker
     - systemctl start docker
     - pip3 install --ignore-installed docker-compose

    hostname:  Backend
    fqdn:  Backend
    prefer_fqdn_over_hostname: true

    final_message: "The system is finally up, after $UPTIME seconds"
  EOT

  tags = {
    Name = "Backend"
  }
}

