output "rds_url" {
  value = aws_db_instance.eschool.address
}

output "frontend_ip" {
  value = aws_instance.Frontend.public_ip
}

output "backend_ip" {
  value = aws_instance.Backend.public_ip
}
