resource "aws_route53_record" "www" {
  zone_id = "Z100351670MMC7DUD9EN"
  name    = "vg.itcawork.shop"
  type    = "A"
  ttl     = 300
  records = [aws_instance.Frontend.public_ip]
}